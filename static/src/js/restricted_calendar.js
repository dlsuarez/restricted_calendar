openerp.restricted_calendar = function(instance) {

    /* CALENDAR */

    var _t = instance.web._t;
    var Users = new openerp.web.Model('res.users');
    var Events = new openerp.web.Model('calendar.event');

    instance.web_calendar.CalendarView.include({

        remove_event: function(id) {
            var self = this;

            do_removal = function() {
                return $.when(self.dataset.unlink([id])).then(function() {
                    self.$calendar.fullCalendar('removeEvents', id);
                });
            };

            confirm_removal = function() {
                if (self.options.confirm_on_delete) {
                    if (confirm(_t("Are you sure you want to delete this record?"))) {
                        do_removal();
                    }
                } else {
                    do_removal();
                }
            };

            Users.call('has_group', ['base.group_no_one'])
                .done(function(result) {
                    if (result == true) {
                        confirm_removal();
                    } else {
                        Events.query(['id', 'user_id'])
                            .filter([['id', '=', id]])
                            .all().then(function(events) {
                                event_ids = [];
                                for (i in events) {
                                    event_ids.push(events[i].user_id[0]);
                                }
                                if (event_ids.indexOf(instance.client.session.uid) != -1) {
                                    confirm_removal();
                                } else {
                                    alert(_t("Your user has not permission to delete this event"));
                                }
                            });
                    }
                });
        },

        open_event: function(id, title) {
            var self = this;

            if (! this.open_popup_action) {
                var index = this.dataset.get_id_index(id);
                this.dataset.index = index;
                if (this.write_right) {
                    this.do_switch_view('form', null, { mode: "edit" });
                } else {
                    this.do_switch_view('form', null, { mode: "view" });
                }
            }
            else {
                var pop = new instance.web.form.FormOpenPopup(this);
                var id_cast = parseInt(id).toString() == id ? parseInt(id) : id;
                pop.show_element(this.dataset.model, id_cast, this.dataset.get_context(), {
                    title: _.str.sprintf(_t("View: %s"),title),
                    view_id: +this.open_popup_action,
                    res_id: id_cast,
                    target: 'new',
                    readonly:true
                });

               var form_controller = pop.view_form;
               form_controller.on("load_record", self, function(){
                    button_delete = _.str.sprintf("<button class='oe_button oe_bold delme'><span> %s </span></button>",_t("Delete"));
                    button_edit = _.str.sprintf("<button class='oe_button oe_bold editme oe_highlight'><span> %s </span></button>",_t("Edit Event"));

                    add_buttons = function() {
                        pop.$el.closest(".modal").find(".modal-footer").prepend(button_delete);
                        pop.$el.closest(".modal").find(".modal-footer").prepend(button_edit);
                        $('.delme').click(
                            function() {
                                $('.oe_form_button_cancel').trigger('click');
                                self.remove_event(id);
                            }
                        );
                        $('.editme').click(
                            function() {
                                $('.oe_form_button_cancel').trigger('click');
                                self.dataset.index = self.dataset.get_id_index(id);
                                self.do_switch_view('form', null, { mode: "edit" });
                            }
                        );
                    };

                    Users.call('has_group', ['base.group_no_one'])
                        .done(function(result) {
                            if (result == true) {
                                add_buttons();
                            } else {
                                Events.query(['id', 'user_id'])
                                    .filter([['id', '=', id]])
                                    .all().then(function(events) {
                                        event_ids = [];
                                        for (i in events) {
                                            event_ids.push(events[i].user_id[0]);
                                        }
                                        if (event_ids.indexOf(instance.client.session.uid) != -1) {
                                            add_buttons();
                                        }
                                    });
                            }
                        });
               });
            }
            return false;
        }

    });

}