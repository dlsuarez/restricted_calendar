# -*- coding: utf-8 -*-
################################################################
#    License, author and contributors information in:          #
#    __openerp__.py file at the root folder of this module.    #
################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from openerp.addons.calendar.calendar import calendar_id2real_id
from datetime import date, datetime
import logging

class restricted_calendar_event(models.Model):

    _inherit = 'calendar.event'

    _logger = logging.getLogger(__name__)

    _defaults = {
        'class': 'private'
    }

    @api.model
    def _get_user_ids_by_group(self, group_xml_id):
        users = self.env['res.users'].search([("id", "=", -1)])
        for user in self.env['res.users'].search([]):
            for group in user.groups_id:
                if self.env.ref(group_xml_id).id == group.id:
                    users = users + user
                for implied_group in group.implied_ids:
                    if self.env.ref(group_xml_id).id == implied_group.id:
                        users = users + user
        return users

    @api.model
    def create(self, values):
        valid_start_date = False
        valid_stop_date = False
        current_user = self.env['res.users'].search([("id", "=", self.env.uid)])[0]
        current_user_belongs_to_manager = False
        user_domain = self.env['res.users'].search([("id", "=", -1)])
        group_manager_users = \
            self._get_user_ids_by_group(
                'base.group_no_one')
        for group in current_user.groups_id:
            if self.env.ref('base.group_no_one').id == group.id:
                current_user_belongs_to_manager = True
        stop = values.get('stop', False)
        start = values.get('start', False)
        stop_date = values.get('stop_date', False)
        start_date = values.get('start_date', False)
        if start and stop:
            valid_start_date = datetime.strptime(start, "%Y-%m-%d %H:%M:%S").date() >= date.today()
            valid_stop_date = datetime.strptime(stop, "%Y-%m-%d %H:%M:%S").date() >= date.today()
        elif start_date and stop_date:
            valid_start_date = datetime.strptime(start_date + " 00:00:00", "%Y-%m-%d %H:%M:%S").date() >= date.today()
            valid_stop_date = datetime.strptime(stop_date + " 00:00:00", "%Y-%m-%d %H:%M:%S").date() >= date.today()
        valid_date = valid_start_date and valid_stop_date
        if valid_date == False and current_user_belongs_to_manager == False:
            try:
                return super(restricted_calendar_event, self).create(False)
            except Exception, e:
                raise Warning(_("This user has not permission to create an old event"))
        else:
            return super(restricted_calendar_event, self).create(values)

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        if context is None:
            context = {}
        fields2 = fields and fields[:] or None
        EXTRAFIELDS = ('class', 'user_id', 'duration', 'allday', 'start', 'start_date', 'start_datetime', 'rrule')
        for f in EXTRAFIELDS:
            if fields and (f not in fields):
                fields2.append(f)
        if isinstance(ids, (basestring, int, long)):
            select = [ids]
        else:
            select = ids
        select = map(lambda x: (x, calendar_id2real_id(x)), select)
        result = []
        real_data = super(restricted_calendar_event, self).read(cr, uid, [real_id for calendar_id, real_id in select], fields=fields2, context=context, load=load)
        real_data = dict(zip([x['id'] for x in real_data], real_data))

        for calendar_id, real_id in select:
            res = real_data[real_id].copy()
            ls = calendar_id2real_id(calendar_id, with_date=res and res.get('duration', 0) > 0 and res.get('duration') or 1)
            if not isinstance(ls, (basestring, int, long)) and len(ls) >= 2:
                res['start'] = ls[1]
                res['stop'] = ls[2]

                if res['allday']:
                    res['start_date'] = ls[1]
                    res['stop_date'] = ls[2]
                else:
                    res['start_datetime'] = ls[1]
                    res['stop_datetime'] = ls[2]

                if 'display_time' in fields:
                    res['display_time'] = self._get_display_time(cr, uid, ls[1], ls[2], res['duration'], res['allday'], context=context)

            res['id'] = calendar_id
            result.append(res)

        for r in result:
            r_id = False or r['id']
            event = False or self.browse(cr, uid, r_id, context=context)
            r['name'] = event['name']
            r['partner_ids'] = event['partner_ids'].mapped('id')
            r['color_partner_id'] = 17
            if r['user_id']:
                user_id = type(r['user_id']) in (tuple, list) and r['user_id'][0] or r['user_id']
                if user_id == uid:
                    continue
            if r['class'] == 'private':
                for f in r.keys():
                    if f not in ('id', 'allday', 'start', 'stop', 'duration', 'user_id', 'state', 'interval', 'count', 'recurrent_id_date', 'rrule'):
                        if isinstance(r[f], list):
                            r[f] = []
                        else:
                            r[f] = False
                    if f == 'name':
                        r[f] = _('Busy')

        for r in result:
            for k in EXTRAFIELDS:
                if (k in r) and (fields and (k not in fields)):
                    del r[k]
        if isinstance(ids, (basestring, int, long)):
            return result and result[0] or False

        return result